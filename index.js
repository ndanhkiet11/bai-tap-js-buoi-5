function sapXep() {
    var res;
    var temp;
    var no1 = document.getElementById("no1").value * 1;
    var no2 = document.getElementById("no2").value * 1;
    var no3 = document.getElementById("no3").value * 1;
    if (no1 > no2) {
        temp = no1;
        no1 = no2;
        no2 = temp;
    }
    if (no1 > no3) {
        temp = no1;
        no1 = no3;
        no3 = temp;
    }
    if (no2 > no3) {
        temp = no2;
        no2 = no3;
        no3 = temp;
    }
    res = `👉 ${no1} < ${no2} < ${no3}`;
    document.getElementById("result").innerHTML = res;
}
function chaoHoi() {
    var tenThanhVien = document.getElementById("thanhvien").value * 1;

    switch (tenThanhVien) {
        case 1:
            tenThanhVien = "Bố";
            break;
        case 2:
            tenThanhVien = "Mẹ";
            break;

        case 3:
            tenThanhVien = "Anh Trai";
            break;

        case 4:
            tenThanhVien = "Em Gái";
            break;
        default:
            tenThanhVien = "Người lạ ơi";
    }
    document.getElementById(
        "result2"
    ).innerHTML = `👋 Xin chào ${tenThanhVien}!`;
}
function dem() {
    var countEven = 0;
    var countOdd = 0;
    var no1 = document.getElementById("no11").value * 1;
    var no2 = document.getElementById("no22").value * 1;
    var no3 = document.getElementById("no33").value * 1;
    if (no1 % 2 == 0) countEven++;
    if (no2 % 2 == 0) countEven++;
    if (no3 % 2 == 0) countEven++;
    countOdd = 3 - countEven;
    document.getElementById(
        "result3"
    ).innerHTML = `👉 Có ${countEven} Số chẵn, ${countOdd} Số lẻ`;
}
function duDoan() {
    var duDoan;
    var canh1 = document.getElementById("canh1").value * 1;
    var canh2 = document.getElementById("canh2").value * 1;
    var canh3 = document.getElementById("canh3").value * 1;
    if (canh1 == canh2) {
        duDoan = `👉Hình tam giác cân`;
        if (canh2 == canh3) duDoan = `👉Hình tam giác đều`;
    } else if (
        canh1 * canh1 + canh2 * canh2 == canh3 * canh3 ||
        canh1 * canh1 + canh3 * canh3 == canh2 * canh2 ||
        canh3 * canh3 + canh2 * canh2 == canh1 * canh1
    )
        duDoan = `👉Hình tam giác vuông`;
    else duDoan = `👉Một loại tam giác nào đó`;
    document.getElementById("result4").innerHTML = duDoan;
}

// bai 5
function dateValidate(day, month) {
    if (month == 2 && day > 28)
        return (document.getElementById(
            "result5"
        ).innerHTML = `👉Ngày không xác định`);
    else if (month > 12)
        return (document.getElementById(
            "result5"
        ).innerHTML = `👉Tháng không xác định`);
    else if (day > 30 && thangCo31Ngay(month) == false)
        return (document.getElementById(
            "result5"
        ).innerHTML = `👉Ngày không xác định`);
    else if (day > 31)
        return (document.getElementById(
            "result5"
        ).innerHTML = `👉Ngày không xác định`);
    else return true;
}
function thangCo31Ngay(month) {
    if (
        month == 1 ||
        month == 3 ||
        month == 5 ||
        month == 7 ||
        month == 8 ||
        month == 10 ||
        month == 12
    )
        return true;
    else return false;
}
function tomorrow() {
    var day = document.getElementById("day").value;
    var month = document.getElementById("month").value;
    var year = document.getElementById("year").value;
    if (dateValidate(day, month) == true) {
        if (month == 2) {
            if (day == 28) {
                day = 1;
                month++;
            } else {
                day++;
            }
        } else if (day == 31 && thangCo31Ngay(month)) {
            if (month == 12) {
                day = 1;
                month = 1;
                year++;
            } else {
                day = 1;
                month++;
            }
        } else if (day == 30 && thangCo31Ngay(month) == false) {
            day = 1;
            month++;
        } else {
            day++;
        }
    } else {
        return dateValidate(day, month);
    }
    document.getElementById("result5").innerHTML = `👉 ${day}/${month}/${year}`;
}
function yesterday() {
    var day = document.getElementById("day").value;
    var month = document.getElementById("month").value;
    var year = document.getElementById("year").value;
    if (dateValidate(day, month) == true) {
        if (day == 1 && month == 1) {
            day = 31;
            month = 12;
            year--;
        } else if (day == 1) {
            month--;
            if (month == 2) {
                day = 28;
                month = 2;
            } else if (thangCo31Ngay(month)) {
                day = 31;
            } else day = 30;
        } else {
            day--;
        }
    } else {
        return dateValidate(day, month);
    }
    document.getElementById("result5").innerHTML = `👉 ${day}/${month}/${year}`;
}
// bai 6

function tinhNgay() {
    var day;
    var month = document.getElementById("month6").value;
    var year = document.getElementById("year6").value;
    if (month > 12 || month < 1)
        return (document.getElementById(
            "result6"
        ).innerHTML = `👉 Tháng không xác định`);
    else if (month == 2) {
        if (year % 4 == 0 && year % 100 != 0) {
            day = 29;
        } else day = 28;
    } else if (thangCo31Ngay(month)) day = 31;
    else day = 30;

    document.getElementById(
        "result6"
    ).innerHTML = `👉 Tháng ${month} năm ${year} có ${day} ngày`;
}
// bài 7
function numToText(num) {
    var result;
    switch (num) {
        case 1:
            result = "một";
            break;
        case 2:
            result = "hai";
            break;
        case 3:
            result = "ba";
            break;
        case 4:
            result = "bốn";
            break;
        case 5:
            result = "năm";
            break;
        case 6:
            result = "sáu";
            break;
        case 7:
            result = "bảy";
            break;
        case 8:
            result = "tám";
            break;
        case 9:
            result = "chín";
            break;
    }
    return result;
}
function docSo() {
    var result = "";
    var number = document.getElementById("num7").value;
    var hangTram = Math.floor(number / 100);

    number = number % 100;
    var hangChuc = Math.floor(number / 10);
    var hangDonVi = number % 10;
    if (hangChuc == 0 && hangDonVi == 0)
        result += numToText(hangTram) + " trăm ";
    else if (hangChuc == 0 && hangDonVi != 0) {
        result +=
            numToText(hangTram) + " trăm " + " lẻ " + numToText(hangDonVi);
    } else if (hangDonVi == 0) {
        result =
            numToText(hangTram) + " trăm " + numToText(hangChuc) + " mươi ";
    } else {
        result =
            numToText(hangTram) +
            " trăm " +
            numToText(hangChuc) +
            " mươi " +
            numToText(hangDonVi);
    }

    document.getElementById("result7").innerHTML = `👉${result}`;
}
// bài 8
function timSvXaNhat() {
    var tenSv1 = document.getElementById("name1").value;
    var tenSv2 = document.getElementById("name2").value;
    var tenSv3 = document.getElementById("name3").value;
    var xSv1 = document.getElementById("x1").value;
    var ySv1 = document.getElementById("y1").value;
    var xSv2 = document.getElementById("x2").value;
    var ySv2 = document.getElementById("y2").value;
    var xSv3 = document.getElementById("x3").value;
    var ySv3 = document.getElementById("y3").value;
    var xTruong = document.getElementById("x4").value;
    var yTruong = document.getElementById("y4").value;
    var dSv1 = Math.sqrt(
        (xSv1 - xTruong) * (xSv1 - xTruong) +
            (ySv1 - yTruong) * (ySv1 - yTruong)
    );
    var dSv2 = Math.sqrt(
        (xSv2 - xTruong) * (xSv2 - xTruong) +
            (ySv2 - yTruong) * (ySv2 - yTruong)
    );
    var dSv3 = Math.sqrt(
        (xSv3 - xTruong) * (xSv3 - xTruong) +
            (ySv3 - yTruong) * (ySv3 - yTruong)
    );
    if (dSv1 >= dSv2 && dSv1 >= dSv3)
        return (document.getElementById(
            "result8"
        ).innerHTML = `👉Sinh viên xa trường nhất: ${tenSv1}`);
    else if (dSv2 >= dSv1 && dSv2 >= dSv3)
        return (document.getElementById(
            "result8"
        ).innerHTML = `👉Sinh viên xa trường nhất: ${tenSv2}`);
    else
        return (document.getElementById(
            "result8"
        ).innerHTML = `👉Sinh viên xa trường nhất: ${tenSv3}`);
}
